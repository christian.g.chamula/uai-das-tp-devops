﻿using EjemploApiRest.Application;
using EjemploApiRest.Entities;
using EjemploApiRest.Webapi.DTOs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EjemploApiRest.Webapi.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class FootballTeamController : ControllerBase
    {
        IApplication<FootballTeam> _football;
        public FootballTeamController(IApplication<FootballTeam> football)
        {
            _football = football;
        }



        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _football.GetAllAsync());
        }


        [HttpPost]
        public async Task<IActionResult> Save(FootBallTeamDTO dto)
        {
            var f = new FootballTeam()
            {
                Name = dto.Name,
                Score = dto.Score,
                Manager = dto.Manager,
            };
            await _football.SaveAsync(f);
            return Ok(f);
        }


        [HttpPut]
        [Route("{id}")]
        public async Task<IActionResult> Update(int id, FootBallTeamDTO dto)
        {
            if (id == 0 || dto==null) return NotFound();

            var tmp = _football.GetById(id);
            if (tmp != null)
            {
                tmp.Id = id;
                tmp.Manager = dto.Manager;
                tmp.Name = dto.Name;
                tmp.Score = dto.Score;
            }
            await _football.SaveAsync(tmp);
            return Ok(tmp);

        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult Delete(int id)
        {
            if (id == 0) return NotFound();
            
            _football.DeleteAsync(id);
            return Ok();
        }


    }
}
